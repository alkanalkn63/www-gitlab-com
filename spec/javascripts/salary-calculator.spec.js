/* eslint-env jasmine */
/* eslint-disable global-require */
/* global Promise */

describe('the salary calculator', function() {
  var jsdom = require('jsdom');
  var path = require('path');
  var template = require('fs').readFileSync(path.resolve(__dirname, '../../source/includes/salary_calculator_vue.html.erb'));
  var SalaryCalculator;

  var fakeData = {
    contractTypes: [{
      country: 'United States',
      employee_factor: 1,
      entity: 'GitLab Inc'
    }, {
      country: '*',
      contractor_factor: 1.17,
      entity: 'GitLab BV'
    }],
    countryNoHire: [
      'Brazil'
    ],
    currencyExchangeRates: {
      rates_to_usd: [{
        currency_code: 'CAD',
        rate: 0.76088,
        countries: ['Canada']
      }]
    },
    locationFactors: [{
      country: 'United States',
      area: 'Columbus, Ohio',
      locationFactor: 47.36
    }, {
      country: 'Canada',
      area: 'Alberta',
      locationFactor: 36.5
    }],
    roles: [{
      title: 'Backend Engineer',
      levels: 'engineering_ic',
      salary: 160000
    }, {
      title: 'Engineering Management, Quality',
      levels: false,
      salary: 182563
    }],
    roleLevels: {
      engineering_ic: [{
        title: 'Junior',
        factor: 0.8
      }, {
        title: 'Intermediate',
        factor: 1.0,
        is_default: true
      }]
    }
  };


  beforeEach(function() {
    var dom = new jsdom.JSDOM(template);
    var jQuery = require('../../source/javascripts/libs/jquery.min')(dom.window);
    var promise = Promise.resolve(fakeData);

    global.document = dom.window.document;
    global.navigator = dom.window.navigator;
    global.window = dom.window;
    global.window.Vue = require('../../source/javascripts/libs/vue.min');
    global.window.VueSelect = require('../../source/javascripts/libs/vue-select.min');
    global.window.Clipboard = require('../../source/javascripts/libs/clipboard.min');
    global.window.$ = jQuery;

    spyOn(jQuery, 'get').and.callFake(function() {
      return promise;
    });

    SalaryCalculator = require('../../source/javascripts/salary-calculator-vue');

    promise.then(function() {
      SalaryCalculator.currentRole = fakeData.roles[0];
      SalaryCalculator.setRoleLevels();
    });

    return promise;
  });

  describe('methods', function() {
    describe('setRoleLevels', function() {
      it('sets the default level if none are available', function() {
        SalaryCalculator.currentRole = fakeData.roles[1];
        SalaryCalculator.setRoleLevels();

        expect(SalaryCalculator.currentLevel).toEqual({ title: 'N/A', factor: 1 });
      });

      it('sets the current level from the role, if available', function() {
        SalaryCalculator.currentRole = fakeData.roles[0];
        SalaryCalculator.setRoleLevels();

        expect(SalaryCalculator.currentLevel).toEqual(fakeData.roleLevels.engineering_ic[1]);
      });
    });

    describe('setParamsFromUrl', function() {
      it('sets values from the URL', function() {
        SalaryCalculator.setParamsFromUrl('http://example.com/?country=United+States&area=Columbus,+Ohio&level=Junior&experience=Learning+the+role');

        expect(SalaryCalculator.currentCountry).toEqual('United States');
        expect(SalaryCalculator.currentArea.area).toEqual('Columbus, Ohio');
        expect(SalaryCalculator.currentLevel.title).toEqual('Junior');
        expect(SalaryCalculator.currentExperience.label).toEqual('Learning the role');
      });

      it('skips params not in the URL', function(done) {
        SalaryCalculator.currentCountry = null;
        SalaryCalculator.setParamsFromUrl('http://example.com/?level=Intermediate');

        SalaryCalculator.$nextTick(function() {
          expect(SalaryCalculator.currentCountry).toBe(null);
          expect(SalaryCalculator.currentLevel.title).toEqual('Intermediate');

          done();
        });
      });

      it('only sets areas valid for the country', function(done) {
        SalaryCalculator.setParamsFromUrl('http://example.com/?country=Canada&area=Columbus,+Ohio');

        SalaryCalculator.$nextTick(function() {
          expect(SalaryCalculator.currentCountry).toEqual('Canada');
          expect(SalaryCalculator.currentArea).toBe(null);

          done();
        });
      });
    });

    describe('formatAmount', function() {
      it('formats the amount in USD by default', function() {
        expect(SalaryCalculator.formatAmount(1234)).toEqual('$1,234');
      });

      it('formats the amount in another currency code when passed', function() {
        expect(SalaryCalculator.formatAmount(1234, 'AUD')).toEqual('1,234 AUD');
      });
    });

    describe('formatAreaLocationFactor', function() {
      it('formats the number to three decimal places', function() {
        expect(SalaryCalculator.formatAreaLocationFactor({ locationFactor: 0.12345 })).toEqual('0.123');
      });

      it('returns "--" when null is passed', function() {
        expect(SalaryCalculator.formatAreaLocationFactor()).toEqual('--');
      });
    });

    describe('findByCountry', function() {
      it('finds a result matching the country name', function() {
        expect(SalaryCalculator.findByCountry(fakeData.contractTypes, 'United States')).toEqual(fakeData.contractTypes[0]);
      });

      it('falls back to * when there is no match', function() {
        expect(SalaryCalculator.findByCountry(fakeData.contractTypes, 'Somewhere else')).toEqual(fakeData.contractTypes[1]);
      });
    });
  });
});
